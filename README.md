# 金黄色的黄昏

#### 介绍
个人小站分享,wanhaifeng.top

#### 软件架构
软件架构说明
python3.7 + Django==2.1.11 + pillow + django-ckeditor



#### 安装教程

1.  pip3 install -r requirements.txt #安装库
2.  pyhont3 manage.py  migrate #初始化数据库
3.  python3 manage.py   makemigrations myindex #初始化myindex 数据模型
4.  pyhont3 manage.py  migrate #初始化 myindex
5.  pyhont3 manage.py runserver #启动服务

#### 使用说明

1.  首次打开后台登录地址 http://127.0.0.1:8000/admin
    需创建管理账号 python3 manage.py createsuperuser
2.  后台支持富文本编辑,图片上传等.


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
