from django.contrib import admin

# Register your models here.
from .models import SecondaryClassification,TopicsOfSecondaryClassification

admin.site.register(SecondaryClassification)
admin.site.register(TopicsOfSecondaryClassification)