#!/bin/bash
kill -9 `netstat -tlnp |grep 8000 |awk '{print $NF}' |awk -F/ '{print $1}'` >/dev/null 2>&1
sleep 1
nohup python3 manage.py  runserver &
